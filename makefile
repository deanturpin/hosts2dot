all: public/topology.svg

.PHONY: topology.dot
topology.dot:
	./hosts2dot.sh < hosts | tee $@

public/%.svg: %.dot
	dot -T svg $< > $@
